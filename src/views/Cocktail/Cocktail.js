import { Container } from "react-bootstrap";
import Drink from "../../Components/Drinks/Drink";
import "./cocktail.css";

export default function Cocktail() {
  return (
    <Container fluid style={{ padding: "20px" }}>
      <h1
        style={{
          paddingTop: "40px",
          color: "#fff",
          fontWeight: "800",
          letterSpacing: "1px",
          fontSize: "50px",
          marginBottom: "100px",
        }}
      >
        Ricerca Cocktail
      </h1>
      <Container className="content-cocktail">
        <Drink></Drink>
      </Container>
    </Container>
  );
}
