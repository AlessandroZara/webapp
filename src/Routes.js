import {HashRouter as Router, Route, Routes} from 'react-router-dom'
import Home from './views/Home/Home'
import Cocktail from './views/Cocktail/Cocktail'
import SingleCocktail from './Components/Drinks/SingleCocktail'

export default function CustomRoutes(){
    return <Router>
        <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/cocktail" element={<Cocktail/>}/> 
            <Route path="*" element={<div>Not Found</div>}/>

            <Route exact path="/cocktail/:cocktailID" element={<SingleCocktail/>}/> 
        </Routes>
    </Router>
}
