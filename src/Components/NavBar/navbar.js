import { Navbar, Container, Nav } from "react-bootstrap";
import "./navbar.css";

export default function NavBar() {
  return (
    <Navbar bg="light" variant="light" className="navbar">
      <Container>
        <Nav className="me-auto nav">
          <Nav.Link href="/">Home</Nav.Link>
          <Nav.Link href="#/cocktail">Cocktail</Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  );
}
