import {Container} from 'react-bootstrap'
import './footer.css'

export default function Footer(){

    return (
            <Container fluid style={{padding:"0px"}}>
                  <div className="footer"  >
                  <h2 style={{color:"#fff",fontSize:"40px",fontWeight:"bold"}}> Footer</h2>
                  </div>
            </Container>)
}