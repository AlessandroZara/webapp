import { Button, Input } from "@material-ui/core";
import axios from "axios";
import { useFormik } from "formik";
import { useState } from "react";
import CardDrink from "./CardDrink";
import "./drink.css";

export default function DrinkView() {
  const [resultCocktail, setResultCocktail] = useState({
    cocktails: null,
    message: "",
  });

  const validate = (values) => {
    const errors = {};

    if (!values.name) {
      errors.name = "Reuired";
    } else if (values.name.length < 2) {
      errors.name = "Must be > 2";
    }

     return errors

  };

  const formik = useFormik({
    initialValues: {
      name: "",
    },
    validate,
    onSubmit: async function (name) {
      try {
        const url =
          "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=" +
          name.name;
        const res = await axios.get(url);
        if (res.data.drinks) {
          setResultCocktail({ cocktails: res.data.drinks, message: "" });
        } else
          setResultCocktail({
            coaktails: null,
            message: "Nessun cocktail trovato!",
          });
      } catch (e) {
        console.log(e);
      }
    },
  });

  return (
    <div className="myDrinks">
      <div>
        <form onSubmit={formik.handleSubmit}>
          <label
            htmlFor="name"
            style={{
              color: "#fff",
              marginTop: "40px",
              marginRight: "5px",
              fontSize: "30px",
              fontWeight: "bold",
            }}
          >
            Cocktail
          </label>
          <Input
            id="name"
            name="name"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.name}
            style={{ marginLeft: "10px", color: "#fff", fontWeight: "bold" }}
          />
          <Button
            type="submit"
            onClick={formik.handleSubmit}
            style={{ color: "#fff", fontSize: "20px", fontWeight: "bold" }}
          >
            cerca
          </Button>
          {formik.errors.name ? (
            <div className="error" style={{ color: "red", fontWeight: "900" }}>
              {formik.errors.name}
            </div>
          ) : null}
        </form>
      </div>

      <div
        className="cocktail-container"
        style={{ maxwidth: "100% !important" }}
      >
        {resultCocktail.cocktails ? (
          resultCocktail.cocktails.map((elem, index) => {
            return (
              <CardDrink
                cocktailID={elem.idDrink}
                key={index}
                drink={elem.strDrink}
                alcholic={elem.strAlcoholic}
                category={elem.strCategory}
                imgDrink={elem.strDrinkThumb}
              />
            );
          })
        ) : resultCocktail.message ? (
          <h3 style={{ color: "#fff" }}>{resultCocktail.message}</h3>
        ) : null}
      </div>
    </div>
  );
}
