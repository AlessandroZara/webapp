import "./cardDrinks.css";
import {Link} from 'react-router-dom'

const CardDrink = (props) => {
  return (
      //questo è un link, quando click vai a questo indirizzo
    <Link to={`/cocktail/${props.cocktailID}`}  style={{textDecoration: 'none'}}>
      <div className="drinks">
        <h3 style={{ marginTop: "10px" }}>Cocktail</h3>
        <p style={{ fontSize: "25px" }}>{props.drink}</p>
        <h3>Info</h3>
        <p style={{ marginBottom: "3px" }}>{props.alcholic}</p>
        <p>{props.category}</p>
        <img src={props.imgDrink}></img>
      </div>
    </Link>
  );
};
export default CardDrink;
