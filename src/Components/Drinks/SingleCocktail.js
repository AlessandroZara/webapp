import { React } from "react";
import { useParams } from "react-router-dom";
import useCocktailClientAPI from "../../ClientAPI/UseCocktailClientAPI";
import { useEffect } from "react";

import { CircularProgress } from "@material-ui/core";
import "./singleCocktail.css";
import InfoSingleCocktail from "./InfoSingleCocktail";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

export default function SingleCocktail() {
  const { cocktailID } = useParams();

  const clientCocktail = useCocktailClientAPI();

  useEffect(() => {
    clientCocktail.fetchDrink(cocktailID);
  }, []);

  if (clientCocktail.loading) {
    return (
      <Container fluid="md">
        <Row>
          <Col>
            <CircularProgress />
          </Col>
        </Row>
      </Container>
    );
  }

  if (clientCocktail.error) {
    return (
      <Container fluid="md">
        <Row>
          <Col>Error: {clientCocktail.error.message}</Col>
        </Row>
      </Container>
    );
  }

  if (!clientCocktail.data || clientCocktail.data.length == 0) {
    return (
      <Container fluid="md">
        <Row>
          <Col>Non ci sono info</Col>
        </Row>
      </Container>
    );
  }

  return (
    <>
      {clientCocktail.data.map((elem) => {
        return (
          <>
            <InfoSingleCocktail
              imgDrink={elem.strDrinkThumb}
              name={elem.strDrink}
              category={elem.strCategory}
              alcholic={elem.strAlcoholic}
              glass={elem.strGlass}
              ingredient1={elem.strIngredient1}
              ingredient2={elem.strIngredient2}
              ingredient3={elem.strIngredient3}
              ingredient4={elem.strIngredient4}
              ingredient5={elem.strIngredient5}
              ingredient6={elem.strIngredient6}
              ingredient7={elem.strIngredient7}
              ingredient8={elem.strIngredient8}
              ingredient9={elem.strIngredient9}
              ingredient10={elem.strIngredient10}
              ingredient11={elem.strIngredient11}
              ingredient12={elem.strIngredient12}
              ingredient13={elem.strIngredient13}
              ingredient14={elem.strIngredient14}
              ingredient15={elem.strIngredient15}
              fluid
            />
          </>
        );
      })}
    </>
  );
}
