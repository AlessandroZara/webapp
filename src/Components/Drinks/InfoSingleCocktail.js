import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import Button from "react-bootstrap/Button";
import "./infoSingleCocktail.css";
import { Link } from "react-router-dom";

export default function InfoSingleCocktail(props) {
  const vett = [];
  vett.push(
    props.ingredient1,
    props.ingredient2,
    props.ingredient3,
    props.ingredient4,
    props.ingredient5,
    props.ingredient6,
    props.ingredient7,
    props.ingredient8,
    props.ingredient9,
    props.ingredient10,
    props.ingredient11,
    props.ingredient12,
    props.ingredient13,
    props.ingredient14,
    props.ingredient15
  );

  return (
    <Container
      fluid
      className="container-singleCocktail d-flex flex-column justify-content-center"
      style={{ color: "white" }}
    >
      <Row>
        <Col>
          <h1
            style={{
              textAlign: "center",
              fontWeight: "bold",
              fontSize: "50px",
            }}
          >
            {props.name}
          </h1>
        </Col>
      </Row>
      <Row className="spazio" />
      <Row>
        <Col
          className="container-img-cocktail d-flex  align-items-center"
          sm={7}
        >
          <Image
            src={props.imgDrink}
            style={{
              borderRadius: "5%",
            }}
            fluid
            className="img-cocktail"
          />
        </Col>
        <Col
          className="container-info-cocktail d-flex  flex-column"
          style={{ fontSize: "20px" }}
          sm={5}
        >
          <h4>Tipo di cocktail</h4>
          <ul>
            <li>{props.category}</li>
            <li>{props.alcholic}</li>
          </ul>
          <h4>Ingredienti</h4>
          <ul>
            {vett.map((elem) => { 
              return (elem ? <li>{elem}</li>: null)
              // if (elem != null) return <li>{elem}</li>;
            })}
          </ul>
          <h4>Bicchiere consigliato</h4>
          <ul>
            <li>{props.glass}</li>
          </ul>
        </Col>
      </Row>
      <Row className="spazio" />
      <Row>
        <Col className="d-flex justify-content-center">
          <Link to={`/cocktail`} style={{ textDecoration: "none" }}>
            <Button
              variant="outlined"
              style={{
                color: "#fff",
                border: "1px solid #fff",
                fontWeight: "bold",
                fontSize: "18px",
              }}
            >
              Indietro
            </Button>
          </Link>
        </Col>
      </Row>
    </Container>
  );
}
