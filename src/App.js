import './app.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import Routes from './Routes';
import Navbar from './Components/NavBar/navbar'
import Footer from './Components/footer/footer';


function App() {
  return (
    <>
     <Navbar/>
      <div className="background"> 
        <div className="linear-gradient">
          <Routes/>
        </div>
      </div>
      <Footer/>
    </>
  )
}

export default App;

