import axios from 'axios'
import { useState } from 'react';

function UseCocktailClientAPI(){ //useCocktailClientAPI è un hook 

    //state loading
    const [loading,setLoading]=useState(true)
    //data
    const [data,setData]=useState(undefined)
    //error
    const [error,setError]=useState(undefined)

    const fetchDrink = async (ID)=>{
        try{
            setLoading(true)
            setError(undefined)
            const result = await axios({
                baseURL:"https://www.thecocktaildb.com/api",
                url:"/json/v1/1/lookup.php?i="+ID,
                method:"GET",
            })
            setData(result.data.drinks)
        }catch(e){
            console.log(e)
            setError(e)
        }finally{
            setLoading(false)
        }
    }

    return {
        fetchDrink,
        loading,
        data,
        error
    }
}

export default UseCocktailClientAPI